import cv2
import matplotlib.pyplot as plt
import numpy as np
from skimage.measure import label, regionprops
from skimage.color import rgb2hsv

image = cv2.imread('balls_and_rects.png')
image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
image_hsv = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
hue = image_hsv[:, :, 0]

_, binary_image = cv2.threshold(hue, 0, 255, cv2.THRESH_BINARY)
labeled = label(binary_image)
areas = regionprops(labeled)

rectangle_count = {}
circle_count = {}
for region in areas:
    area = region.area
    proportion = region.major_axis_length / region.minor_axis_length

    if proportion > 1.2:
        shape = 'rectangle'
    else:
        shape = 'circle'

    hue_value = np.mean(hue[labeled == region.label])
    if shape == 'rectangle':
        if hue_value in rectangle_count:
            rectangle_count[hue_value] += 1
        else:
            rectangle_count[hue_value] = 1
    elif shape == 'circle':
        if hue_value in circle_count:
            circle_count[hue_value] += 1
        else:
            circle_count[hue_value] = 1

print('Общее количество фигур:', len(areas))
print('Количество прямоугольников каждого оттенка:')
for hue_value, count in rectangle_count.items():
    print('Оттенок:', hue_value, 'Количество:', count)
print('Количество кругов каждого оттенка:')
for hue_value, count in circle_count.items():
    print('Оттенок:', hue_value, 'Количество:', count)

plt.imshow(labeled)
plt.show()