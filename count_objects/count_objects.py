import numpy as np
import matplotlib.pyplot as plt
from skimage.morphology import (binary_erosion, binary_dilation, binary_opening, binary_closing)
from skimage.measure import label


ps = np.load(r"ps.npy.txt")
ps_label = label(ps)
all = ps_label.max()

object1 = np.array([[0,0,0,0,0,0],
                    [0,0,0,0,0,0],
                    [1,1,1,1,1,1],
                    [1,1,1,1,1,1],
                    [1,1,1,1,1,1],
                    [1,1,1,1,1,1]])

object2 = np.array([[0,0,0,0,0,0],
                    [0,0,0,0,0,0],
                    [1,1,0,0,1,1],
                    [1,1,0,0,1,1],
                    [1,1,1,1,1,1],
                    [1,1,1,1,1,1]])

object3 = np.array([[0,0,0,0,0,0],
                    [0,0,0,0,0,0],
                    [1,1,1,1,1,1],
                    [1,1,1,1,1,1],
                    [1,1,0,0,1,1],
                    [1,1,0,0,1,1]])

object4 = np.array([[0,0,1,1,1,1],
                    [0,0,1,1,1,1],
                    [0,0,0,0,1,1],
                    [0,0,0,0,1,1],
                    [0,0,1,1,1,1],
                    [0,0,1,1,1,1]])

object5 = np.array([[0,0,1,1,1,1],
                    [0,0,1,1,1,1],
                    [0,0,1,1,0,0],
                    [0,0,1,1,0,0],
                    [0,0,1,1,1,1],
                    [0,0,1,1,1,1]])

count_object1 = label(binary_opening(ps_label, object1)).max()
count_object2 = label(binary_opening(ps_label, object2)).max() - label(binary_opening(ps_label, object1)).max()
count_object3 = label(binary_opening(ps_label, object3)).max() - label(binary_opening(ps_label, object1)).max()
count_object4 = label(binary_opening(ps_label, object4)).max()
count_object5 = label(binary_opening(ps_label, object5)).max()

print('Общее количество объектов: ', all)
print('Количество объектов для первого вида: ', count_object1)
print('Количество объектов для второго вида: ', count_object2)
print('Количество объектов для третьего вида: ', count_object3)
print('Количество объектов для четвертого вида: ', count_object4)
print('Количество объектов для пятого вида: ', count_object5)

plt.imshow(ps)
plt.show()