import matplotlib.pyplot as plt
import numpy as np
from skimage.measure import label, regionprops
from operator import attrgetter

obj1, obj2 = [], []

for i in range(0, 100):
    image = np.load(f'out/h_{i}.npy')
    labeled = label(image)
    areas = regionprops(labeled)

    if len(areas) == 2:
        sort_areas = areas.sort(key=attrgetter('area'),
                                reverse=True)
        obj1.append(areas[0].centroid)
        obj2.append(areas[1].centroid)

obj1, obj2 = np.array(obj1), np.array(obj2)

plt.plot(obj1[:, 0], obj1[:, 1], color='black')
plt.plot(obj2[:, 0], obj2[:, 1], color='pink')
plt.show()

