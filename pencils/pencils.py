import numpy as np
import matplotlib.pyplot as plt
from skimage.measure import label, regionprops
from skimage.morphology import binary_closing

images = 12
pencils = []

for i in range(1, images + 1):
    pencils_count = 0
    min_area = 255555
    image = plt.imread(f'images/img ({i}).jpg')
    image = np.mean(image, 2)

    image = np.where(image > 128, 0, image)
    image = np.where(image != 0, 1, image)

    closed = binary_closing(image)
    labeled = label(closed)
    areas = regionprops(labeled)

    for area in areas:
        if area.area > min_area:
            if abs(1 - round(area.eccentricity, 2)) < 0.01:
                pencils.append(area)
                pencils_count += 1

all = len(pencils)
print('Всего карандашей:', all)
