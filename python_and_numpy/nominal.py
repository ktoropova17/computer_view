import numpy as np

def nominal(image, mm):
    pixel = []
    for i in image:
        if 1 in i:
            pixel.append(i)
    if (len(pixel)) != 0:
        res = mm / len(pixel)
    else:
        return 0
    return res

for i in range(1, 6):
    with open(f"files/figure{i}.txt") as f:
        mm = float(f.readline())
        f.readline()
        image = []
        for line in f:
            image.append([int(x) for x in line.split()])
        print(nominal(image, mm))
