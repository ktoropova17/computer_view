import numpy as np

def offset(img):
    with open(img, "r") as file:
        lines = file.readlines()[2:]
    image = []
    for line in lines:
        image.append(list(map(int, line.split())))
    for y in range(0, len(image)):
        for x in range(0, len(image[y])):
            if image[y][x] == 1:
                return x, y

x, y = offset("files/img1.txt")
x1, y1 = offset("files/img2.txt")
print("Смещение y:", y1 - y, "Смещение x:", x1 - x)