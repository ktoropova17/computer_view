import matplotlib.pyplot as plt
import numpy as np
from skimage.measure import label, regionprops
from skimage import draw

def has_vline(arr):
    return 1.0 in arr.mean(0)

def recognize(prop):
    euler_number = prop.euler_number
    if euler_number == -1: #две дырки, 8 или B
        if has_vline(prop.image):
            return 'B'
        else:
            return '8'
    elif euler_number == 0:
        y, x = prop.centroid_local
        y /= prop.image.shape[0]
        x /= prop.image.shape[1]
        if np.isclose(x, y, 0.04):
            if has_vline(prop.image):
                return 'P'
            else:
                return '0'
        else:
            if has_vline(prop.image):
                return 'D'
            else:
                return 'A'
    else: # нет дырок, 1 W X * - /
        if prop.image.mean() == 1.0:
            return '-'
        else: # 1 W X * /
            if has_vline(prop.image) and has_vline(prop.image.T):
                return '1'
            else:
                tmp = prop.image.copy()
                tmp[[0, -1], :] = 1
                tmp[:, [0, -1]] = 1
                tmp_labeled = label(tmp)
                tmp_props = regionprops(tmp_labeled)
                tmp_euler = tmp_props[0].euler_number
                if tmp_euler == -3:
                    return 'X'
                elif tmp_euler == -1:
                    return '/'
                else:
                    # print()
                    if prop.eccentricity > 0.5:
                        return 'W'
                    else:
                        return '*'
    return '_'

image = plt.imread(r'symbols.png')
image = image.mean(2)
image = image > 0
labeled = label(image)

props = regionprops(labeled)
result = {}
for prop in props:
    symbol = recognize(prop)
    if symbol not in result:
        result[symbol] = 0
    result[symbol] += 1
print('Проценты распознавания символов:', result)

plt.imshow(labeled)
plt.show()